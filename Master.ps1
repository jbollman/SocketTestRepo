﻿function Get-SocketTest {
    [CmdletBinding()]
        param (
               [Parameter(Mandatory = $True)]
               [string[]]$servers,
               
               [Parameter(Mandatory = $True)]
               [int[]]$ports,

               [Parameter(Mandatory = $True)]
               [string]$User,
              
              [Parameter(Mandatory = $True)]
              [string]$Password
        )

        $secpassword = ConvertTo-SecureString ($Password) -AsPlainText -force
        $credential = New-Object -TypeName System.Management.Automation.PSCredential -ArgumentList $User, $secpassword


 $ExportObject = @()  #Empty array created.  Will be used to gather all like objects, and exported as a collection of objects.
    foreach ($server in $servers) {
            Try {
                 Invoke-Command -ComputerName $server -ArgumentList $servers,$ports,$server `
                                   -Credential $credential -ErrorAction Stop `
                                   -asjob -ScriptBlock {
                                                    $servers = $args[0]
                                                    $ports =  $args[1]
                                                    $computername = $args[2]     #points to the actual $server inside the foreach.  The enumerated server.
                                                    foreach ($server in $servers) {
                                                        if ($server -eq $computername){
                                                            continue                                          
                                                        }
                                                    foreach ($port in $ports) {
                                                                     #chose Test-netconnection over test-connection, because it would allow you to specify a port.  Since -computername and -port does not accept an array, 
                                                                     #i Used 2 foreach, 1 nested in the other.                             
                                                        $CommandTest = Test-NetConnection -Port $port -ComputerName $server

                                                        $Props = @{
                                                        'Computer Name' = $CommandTest.ComputerName
                                                        'Remote Address' = $server
                                                        'Source Address' = $computername
                                                        'Ping Succeeded' = $CommandTest.Pingsucceeded
                                                        'TcpTestsuccess' = $CommandTest.TcpTestSucceeded
                                                        'Remote Port' = $port
                                                        'Error Message' = 'N/A'
                                                        }
                                                        $obj = New-Object -TypeName PSobject -Property $Props
                                                        write $obj         # write-output $obj will define the newly created object, and its content will be captured insdie of the background job processes
                                                }  #foreach port
                                        
                                            }  #foreach server
                               
                                   }   #scriptblock
            }  
            Catch {

                    $Props = @{
                                'Computer Name' = $server
                                'Remote Address' = $null
                                'Source Address' = $null
                                'Ping Succeeded' = $null
                                'TcpTestsuccess' = $null
                                'Remote Port' = $null
                                'Error Message' = $_.Exception.Message
                             }   
                          
                   $FailedObject = New-Object PSObject -Property $Props
                   $ExportObject += $FailedObject                  # Adding the contents of $FailedObject to $ExportObject
            }  

    }  

            $TotalTime = 0
        While (((Get-Job).State -contains "Running")) {
           cls
           Write-Host "$((Get-Job | Where {$_.State -NE "Running"}).Count) Of $((Get-Job).Count) Job$(If((Get-Job).Count -GT 1){'s'}) Completed"
           Write-host "$TotalTime Minutes elapsed."


           If ($TotalTime -gt 120) {
                Break
           }  #IF

           Start-Sleep -Seconds 60
           $TotalTime++
        }  # While
        ForEach ($Job In (Get-Job)) {
            $ExportObject += Receive-Job $Job
        }   # ForEach is ran once all Job instances are not in a "Running" state
            $ExportObject | select 'Computer Name','Remote Address','Source Address','Ping Succeeded','TcpTestSuccess','Remote Port','Error Message' | Export-Csv -Path "$env:USERPROFILE\documents\results.csv" -Force -NoTypeInformation
}
