function Append-Log {
    param (
        [Parameter(ValueFromPipeline=$True)][string]$inputString
    )
    $inputString | tee-object $Logs -append
}

function Send-ToLog  {
    param (
        [Parameter(ValueFromPipeline=$True)]$inputString,
        [switch]$Report,
        [switch]$LogFile,
        [switch]$LogStash,
        [switch]$Jenkins,
        [ValidateSet("red","cyan","yellow","white","green")]$WriteColor
    )
    if ($WriteColor -and $global:JenkinsScriptName -eq $null){write-host -f $WriteColor $inputString}
    if ($Report){$inputString >> $Reports}
    if ($LogFile){$inputString >> $Logs}
    if ($LogStash){
        $message = $inputString.split(",")[0]
        $result = $inputString.split(",")[1]

        $JSON = $data | convertto-json

        if ($global:LogstashStatus){
            $message = $inputString.split(",")[0]
            $result = $inputString.split(",")[1]

            $data = @{
                build_id = $Build_ID;
                build_geo = $($Properties.geoLocation);
                build_dc = $($Properties.datacenter);
                message = $message;
                result = $result;
                log_type = $($Properties.log_type);
                qa_type = $($Properties.qa_Type);
                qa_category = $($Properties.qa_Category);
            }

            $JSON = $data | convertto-json

            $headers = @{ Authorization = $("Basic $([System.Convert]::ToBase64String($([System.Text.Encoding]::ASCII.GetBytes("$("devops:$([Runtime.InteropServices.Marshal]::PtrToStringAuto([Runtime.InteropServices.Marshal]::SecureStringToBSTR($(ConvertTo-SecureString "76492d1116743f0423413b16050a5345MgB8AFcAKwBIAGoAeQBqAFcARQAzAEMAeQA5AFUAWAB5ADkALwBNADcAegBBAFEAPQA9AHwAOABmAGIANgBmAGIAMwA2AGQAMQBmADgAZQA3ADAANAA3AGYAYwA0ADAANQA2ADcAOQA3ADMAOQA5ADQAMwBkADAAYgBlAGYANwA1AGIAMQBjADEAZgA0ADEANQA2ADQAZgAyADUAZAA1AGMAZAAyAGMANgBjAGUAZQBlAGIAZQA=" -key (1..32)))))")"))))") }

            if ($global:LogstashStatus){
                try{
                    $response = Invoke-WebRequest -URI $Properties.logServer -ContentType "application/json" -Method Post -Body $JSON -UseBasicParsing -headers $headers
                }
                catch{
                    $Global:LogstashStatus = $False
                    "Logstash appears to be down. Please contact the Dimension Data DevOps team to correct the issue."
                }
                if ($response.StatusCode -ne 200){
                    $Global:LogstashStatus = $False
                    "Logstash appears to be down. Please contact the Dimension Data DevOps team to correct the issue."
                }
            }
        }
    }
    if ($Jenkins -and $global:JenkinsScriptName -ne $null){return $inputString}
    if (($inputString -match "FAIL") -AND ($WriteColor -eq "red")){
        $global:FailCounter ++
    }
}

function Test-IsNotNullOrEmpty {
    param (
        [Parameter(ValueFromPipeline=$True)]$inputValue
    )
    if ($inputValue -eq $null -or $inputValue -eq ""){return $false}
    else {return $true}
}

function Create-ConfigVariables {
    $global:Properties = @{}
    $config = gc -raw ./Config/Config.json | convertfrom-json
    $subtreesInConfig = ($config | gm -MemberType noteproperty).name
    foreach ($subtree in $subtreesInConfig){
        $branchesInSubtree = ($config.$subtree | gm -MemberType noteproperty).name
        foreach ($branch in $branchesInSubtree) {
            $global:Properties += @{$($branch) = $($config.$subtree.$branch)}
        }
    }
    foreach ($property in ($global:properties.keys.split("`n"))){
        if ((gci Env:\$property -ea 0) -ne $null){
            $global:Properties.$property =(gci Env:\$property).value
        }
        else {Approve-LastError}
    }
    foreach ($property in ($global:properties.keys.split("`n"))){
        if ((get-variable $property -scope global -ea 0) -ne $null){
            $global:Properties.$property = (get-variable $property -scope global).value
        }
        else {Approve-LastError}
    }
}

function Set-PropertyInSplat {
    param (
        [ValidateNotNullOrEmpty()][string]$propertyName,
        [ValidateNotNullOrEmpty()]$propertyValue
    )
    if (-not $($global:Properties.keys -match "^$($propertyName)$")){
        $global:Properties.Add($propertyName,$propertyValue)
    }
    else {$global:Properties.$propertyName = $propertyValue}
}

function Approve-LastError {
    start-sleep -milliseconds 50
    if ($error[0] -ne $null){$error.RemoveAt(0)}
}
