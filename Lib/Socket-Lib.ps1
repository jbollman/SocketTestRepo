﻿class SocketTest {
    [string]$hostname
    [PSCredential]$localAdminCred
    [string]$IPaddress
    [System.Management.Automation.Runspaces.PSSession]$session
    [string]$tempDrive
    [int]$Port

    SocketTest ($localAdminCred,$IPaddress){
        $this.localAdminCred = $localAdminCred
        $this.IPaddress = $IPaddress
    }

    [void] CreateSession(){
        if (-not ($this.session.state -eq "Opened")){
            if ($this.session){
                $this.session | Remove-PSSession
            } 
            "`n Creating new session on $($this.IPaddress)..." | Send-ToLog -LogF -W White -J

            $TrustedHosts = (Get-item WSMan:\localhost\Client\TrustedHosts).Value
            if (-not ($TrustedHosts -eq "*")){
                set-item wsman:\localhost\Client\TrustedHosts -value * -Force | out-null  
            }
            $this.Session = New-PSSession -ComputerName $this.IPaddress -Credential $this.localAdminCred
        }
    }
    [void] CloseSession(){
        if ($this.session){
            $this.session | Remove-PSSession
        }
    }
    [void] MakeTemp(){
        $this.GetHostname()
        $remoteDrive = "MHPrep"
        if (-not (Get-PSDrive -Name $remoteDrive -ea 0)){
            "`nCreating temp drive on $($this.hostname)..." | Send-ToLog -LogF -W White -J

            $uncPath = IPaddressTo-UNC $($this.ipaddress)
            New-PsDrive -Name $remoteDrive -PSProvider filesystem -Root \\$uncPath\C$ -Credential $this.localAdminCred -scope Script
        }
        if (-not (test-path "$($remoteDrive):\temp")){
            mkdir "$($remoteDrive):\temp" | out-null
        }
        $this.tempDrive = "$($remoteDrive):\temp"
    }
    [void] RemoveTemp(){
        if ($this.tempDrive){
            $driveName = $this.tempDrive.replace(":\temp","")
            Get-PSDrive | ? {$_.Name -like $driveName} | remove-psdrive -Force 
        }
    }
    [void] SetRemoteExecutionPolicy(){
        $this.CreateSession()

        $CurrentPolicy = invoke-command -session $this.session -scriptblock {Get-ExecutionPolicy -Scope CurrentUser}

        if (-not ($CurrentPolicy -eq "Unrestricted")){

            invoke-command -session $this.session -scriptblock {Set-ExecutionPolicy Unrestricted -Scope CurrentUser -Force}
        }
    }
    [void] GetHostname(){
        if (-not ($this.hostname)){
            $this.hostname = (Get-WmiObject -Class Win32_ComputerSystem -ComputerName $this.ipaddress -Credential $this.localAdminCred).Name
        }
    }
    [void] GetSocket($Portlistarray,$IPaddressarray){
        $this.GetHostname()
        $this.CreateSession()

        $result = Invoke-Command -Session $this.session -ArgumentList $Portlistarray,$IPaddressarray,$this.ipaddress -ScriptBlock { 
            $WarningPreference = 'SilentlyContinue'
            $Portlistarray = $args[0]
            $IPaddressarray = $args[1]
            $computername = $args[2]
            foreach ($IPaddress in $IPaddressarray){
                  if ($IPaddress-eq $computername){
                        continue
                  }
                foreach ($Port in $Portlistarray){
                    if ($Port -eq $null){
                        continue
                    }
                    Test-NetConnection -Port $Port -ComputerName $IPaddress
                }
            }

        }
        write $result
        foreach ($instance in $result){

            if ($instance.TcpTestSucceeded){
                "Source Address $($instance.sourceaddress.ipaddress) was able to reach $($instance.RemoteAddress) on Port $($instance.RemotePort),PASS" | Send-ToLog -R -LogS -W Green -J
            }
            else {
                "Source Address $($instance.sourceaddress.ipaddress) was NOT able to reach $($instance.RemoteAddress) on Port $($instance.RemotePort),FAIL" | Send-ToLog -R -LogS -W Red -J
            }
        }

    } 
}