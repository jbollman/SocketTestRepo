function Install-ModuleFromZip {
    param(
        [ValidateNotNullorEmpty()][string]$archive,
        [ValidateNotNullorEmpty()][string]$moduleName,
        [Switch]$IsRemote
    )

    if (-not (Get-Module -Name $moduleName)){
        if ($IsRemote){
            $file = get-item -path $archive
        }
        else{
            $file = Get-Item -Path ./modules/$archive
        }
        $file | unblock-file
        $targets =  @("$($env:USERPROFILE)\Documents\WindowsPowerShell\Modules","$($env:WINDIR)\System32\WindowsPowerShell\v1.0\Modules")

        foreach($targetondisk in $targets){
            New-Item -ItemType Directory -Force -Path $targetondisk | out-null
            $shell_app= new-object -com shell.application
            $zip_file = $shell_app.namespace("$file")
            $destination = $shell_app.namespace($targetondisk)
            $destination.Copyhere($zip_file.items(), 0x10)
            if(-not (Test-Path($targetondisk + "\" + $moduleName))){
                Rename-Item -Path ($targetondisk + "\" + $($file.BaseName)) -NewName "$moduleName" -Force | out-null
            }
        }
    }
}

function IPaddressTo-UNC {
    param (
        [string]$IP
    )

    try {
        $iptype = ([ipaddress]$IP).AddressFamily
    }
    catch{
        "invalid IP address"
    }
    if ($iptype -eq "InterNetwork"){
        return $IP
    }
    elseif ($iptype -eq "InterNetworkV6") {
        $IP = $($IP.replace(":","-").replace("%","s")) + ".ipv6-literal.net"
        return $IP
    }
}