﻿$serverList = import-csv -Path ".\config\$($Properties.ServerListCsv)"


$WarningPreference = 'SilentlyContinue'
$global:localAdminCred = New-Object PSCredential $($Properties.baseAdminUser),$($properties.baseAdminPassword | ConvertTo-SecureString -AsPlainText -Force)
if(-not ($properties.domainUser -eq "")){
    $global:domainCred = New-Object PSCredential $($Properties.domainUser),$($properties.domainPassword | ConvertTo-SecureString -AsPlainText -Force)
}


$count = 0
foreach ($desiredServer in $serverList){
   
            $actualServer = [SocketTest]::new($localAdminCred,$($desiredServer.IPaddress))
    
            $actualServer.SetRemoteExecutionPolicy()

            $actualServer.GetSocket($serverList.Port,$serverList.IPaddress)
                 
            $actualServer.CloseSession()
            $actualServer.RemoveTemp()
            $count ++

            if ($count -lt $ServerList.Count){
                "`nSocket Test is complete for $($actualServer.hostname). `nBeginning Test for next server..." | Send-ToLog -LogF -W White -J
            }
            else {
                "`nSocket Test is complete for $($actualServer.hostname)" | Send-ToLog -LogF -W White -J 
            }
        
 }