﻿function getposh{

    if ($PSVersionTable.PSVersion.Major -lt 5){
    return "Install WMF 5.0"
    }
    Try {
        Install-PackageProvider -Name NuGet -MinimumVersion 2.8.5.201 -Force -ErrorAction stop | out-null
        Set-PSRepository -InstallationPolicy Trusted -name PSGallery -ErrorAction stop
    }
    Catch {
        $_.ExceptionMessage
    }
    Try {
            if ((get-module posh-ssh).version -ne '1.7.3'){find-module posh-ssh -RequiredVersion '1.7.3' | install-module}
            import-module posh-ssh
    }
    Catch {
        $_.ExceptionMessage
    }
    $posh = get-module -name "Posh-SSH"
    if ($posh){
        write 'Posh installed'
    }
    else {
        write 'Posh not installed'
    
    }
}
getposh
function Install-ModuleFromZip {
    param(
        [ValidateNotNullorEmpty()][string]$archive,
        [ValidateNotNullorEmpty()][string]$moduleName
    )

    if (-not (Get-Module -Name $moduleName)){
        $file = Get-Item -Path ./modules/$archive
        $targetondisk = "$($env:USERPROFILE)\Documents\WindowsPowerShell\Modules"

        New-Item -ItemType Directory -Force -Path $targetondisk | out-null
        $shell_app= new-object -com shell.application
        $zip_file = $shell_app.namespace("$file")
        $destination = $shell_app.namespace($targetondisk)
        $destination.Copyhere($zip_file.items(), 0x10)

        if(-not (Test-Path($targetondisk + "\" + $moduleName))){
            Rename-Item -Path ($targetondisk + "\" + $($file.BaseName)) -NewName "$moduleName" -Force | out-null
        }
        Import-Module -Name $moduleName | out-null
    }
    if (Get-Module -Name $moduleName){return $true}
    return $false
}

#Install-ModuleFromZip -archive "Posh-SSH-master.zip" -moduleName "Posh-SSH" | Out-Null

#iex (New-Object Net.WebClient).DownloadString("https://gist.github.com/darkoperator/6152630/raw/c67de4f7cd780ba367cccbc2593f38d18ce6df89/instposhsshdev")


