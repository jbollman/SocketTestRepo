﻿if ($psversiontable.psversion.major -lt 5){
    write-host -f cyan "You need powershell version 5 or higher. Please download and install .Net 4.5 and WMF 5."
    read-host Exit
    exit
}

if ((New-Object Security.Principal.WindowsPrincipal $([Security.Principal.WindowsIdentity]::GetCurrent())).IsInRole([Security.Principal.WindowsBuiltinRole]::Administrator)  -ne $true){"Not in admin mode"; exit}

if ($Debug) {set-psdebug -trace 2}
else {set-psdebug -off}

if ($global:jenkinsScriptName -eq $null -AND $Debug){$ErrorActionPreference = "Inquire" ; $WarningPreference = "Inquire"}

$WarningPreference = "SilentlyContinue"

$libaries = gci ./lib -recurse -Include *.ps1
foreach ($library in $libaries){
    if ($library.Directory.Name -ne "lib"){
        . ./lib/$($library.Directory.Name)/$($library.Name)
    }
    else {
        . ./lib/$($library.Name)
    }
}

if (-not (test-path ./logs -ea 0)) {mkdir ./Logs | out-null}
Approve-LastError

if (-not (test-path ./Reports -ea 0)) {mkdir ./Reports | out-null}
Approve-LastError

try {Create-ConfigVariables} catch {$error ; read-host "Redundant property name in Config.json, exiting" ; exit}
$Logs = "./logs/" + "Log-$(Get-Date -Format MM-dd-yyyy_HH-mm-ss)" + ".log"
$Reports = "./reports/" + "Report-$(Get-Date -Format MM-dd-yyyy_HH-mm-ss)" + ".csv"
$null > $Logs
"message,result" > $Reports
$Build_ID = "$(Get-Date -Format yyyyMMdd-HH:mm:ss)"
$global:LogstashStatus = $True

$LargeSleep = $Properties.largeSleep
$MediumSleep = $Properties.mediumSleep
$SmallSleep = $Properties.smallSleep

if ($global:JenkinsScriptName -ne $null -or $global:PesterRun -eq $true){
    $ScriptName = $($($pwd.path) + "/scripts/" + $global:JenkinsScriptName + ".ps1")
    if ($global:PesterRun -ne $true){& $ScriptName}
}
else {
    while ($true){
        $MenuChoice = 0
        $CountMenuItems = 0
        remove-variable Menu_* -ea 0 -force
        write-host -f green "`n--------------------Menu Choices--------------------`n"
        foreach ($Script in $(gci ./scripts/*.ps1)){
            $CountMenuItems++
            $FunctionName = [string]$Script.name.replace('.ps1','') -replace ("^\d\d-","")
            write-host -f green $("`t$CountMenuItems" + ". " + "$FunctionName")
            New-Variable "Menu_$($CountMenuItems)" -value $($Script.fullname)
        }
        write-host -f green "`tQ. Quit"
        write-host -f green "`n----------------------------------------------------`n"
        while ($MenuChoice -lt 1 -or $MenuChoice -gt $CountMenuItems) {
            $MenuChoice = read-host Please choose number to execute
            if ($MenuChoice -eq "Q"){exit}
            $ScriptName = $((get-variable "Menu_$($MenuChoice)").value)
            & $ScriptName
        }
    }
}